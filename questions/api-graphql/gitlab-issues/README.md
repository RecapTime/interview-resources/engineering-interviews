# Query open GitLab issues on one of GitLab.org repos by `IssueConnection.createdAt`

* Codelab details: <https://recaptime-dev.notion.site/Query-open-GitLab-issues-on-one-of-GitLab-org-repos-by-IssueConnection-createdAt-1cd093b9baf445719c87ba5bb97b5075>
* Author: Andrei Jiroh Halili (@ajhalili2006)

## GraphQL Query

```graphql
{
  queryComplexity {
    score
    limit
  }
  project(fullPath: "gitlab-org/gitlab") {
    issues(state: opened, sort: CREATED_ASC, first: 30) {
      count
      nodes {
        blockedByCount
        title
        descriptionHtml
        totalTimeSpent
        updatedAt
        createdAt
        dueDate
        upvotes
        downvotes
        weight
        author {
          id
          avatarUrl
          username
          avatarUrl
        }
        epic {
          author {
            id
            avatarUrl
            username
            avatarUrl
          }
          descriptionHtml
          id
          webUrl
          createdAt
          closedAt
          state
        }
        webUrl
        moved
      }
    }
  }
}
```

## Expected Result

```json
{
  "data": {
    "queryComplexity": {
      "score": 54,
      "limit": 250
    },
    "project": {
      "issues": {
        "count": 40497,
        "nodes": [
          {
            "blockedByCount": 0,
            "title": "Provide more user information on currently linked omniauth accounts",
            "descriptionHtml": "<h1 data-sourcepos=\"1:1-1:13\" dir=\"auto\">&#x000A;<a id=\"user-content-the-problem\" class=\"anchor\" href=\"#the-problem\" aria-hidden=\"true\"></a>The Problem</h1>&#x000A;<p data-sourcepos=\"3:1-3:302\" dir=\"auto\">Currently when viewing linked social accounts on a gitlab CE user profile page (ex: <a href=\"https://gitlab.com/profile/account\">https://gitlab.com/profile/account</a> ) the only feedback provided to the user is which account is linked in the form of an extra border around the icon. For example in this screenshot, the github service has been linked:</p>&#x000A;<p data-sourcepos=\"5:1-5:128\" dir=\"auto\"><a class=\"no-attachment-icon\" href=\"https://gitlab.com/uploads/gitlab-org/gitlab-ce/9724a38a7f/screenshot_2014-08-07_19.05.48.png\" target=\"_blank\" rel=\"noopener noreferrer\"><img src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" alt=\"screenshot_2014-08-07_19.05.48\" class=\"lazy\" data-src=\"https://gitlab.com/uploads/gitlab-org/gitlab-ce/9724a38a7f/screenshot_2014-08-07_19.05.48.png\"></a></p>&#x000A;<p data-sourcepos=\"7:1-7:231\" dir=\"auto\">As a user with potentially many google, github, etc. accounts this is not nearly enough information. I would like to be able to see which google account I've linked, and be able to unlink it when necessary (see issue gitlab-ce#477)</p>&#x000A;<h1 data-sourcepos=\"10:1-10:12\" dir=\"auto\">&#x000A;<a id=\"user-content-suggestion\" class=\"anchor\" href=\"#suggestion\" aria-hidden=\"true\"></a>Suggestion</h1>&#x000A;<p data-sourcepos=\"12:1-12:189\" dir=\"auto\">Instead of only showing three icons with an outline, display omniauth services in a table, where each service has its own row. Once linked, a service row should display information such as:</p>&#x000A;<ul data-sourcepos=\"14:1-16:64\" dir=\"auto\">&#x000A;<li data-sourcepos=\"14:1-14:22\">The account username</li>&#x000A;<li data-sourcepos=\"15:1-15:22\">The account avatar</li>&#x000A;<li data-sourcepos=\"16:1-16:64\">An 'Unlink  Account' Button (see issue gitlab-ce#477)</li>&#x000A;</ul>",
            "totalTimeSpent": 0,
            "updatedAt": "2021-06-20T18:18:03Z",
            "createdAt": "2014-08-07T16:24:05Z",
            "dueDate": null,
            "upvotes": 1,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/43581",
              "avatarUrl": "https://secure.gravatar.com/avatar/9fa2131cef896ad0596d4fafc99f09d8?s=80&d=identicon",
              "username": "johannesmp"
            },
            "epic": {
              "author": {
                "id": "gid://gitlab/User/3079878",
                "avatarUrl": "/uploads/-/system/user/avatar/3079878/avatar.png",
                "username": "rayana"
              },
              "descriptionHtml": "<h2 data-sourcepos=\"1:1-1:10\" dir=\"auto\">&#x000A;<a id=\"user-content-purpose\" class=\"anchor\" href=\"#purpose\" aria-hidden=\"true\"></a>Purpose</h2>&#x000A;<p data-sourcepos=\"3:1-3:221\" dir=\"auto\">A bucket to identify new <span class=\"gl-label gl-label-sm\"><a href=\"/groups/gitlab-org/-/epics?label_name=feature\" data-original=\"~feature\" data-link=\"false\" data-link-reference=\"false\" data-group=\"9970\" data-label=\"10230929\" data-reference-type=\"label\" data-container=\"body\" data-placement=\"top\" title=\"Any issue/MR that contains work to support the implementation of a feature and/or results in an improvement in the user experience. Read more at https://about.gitlab.com/handbook/engineering/metrics/#data-classification\" class=\"gfm gfm-label has-tooltip gl-link gl-label-link\"><span class=\"gl-label-text gl-label-text-light\" data-container=\"body\" data-html=\"true\" style=\"background-color: #F0AD4E\">feature</span></a></span> requests in <span class=\"gl-label gl-label-sm\"><a href=\"/groups/gitlab-org/-/epics?label_name=settings\" data-original=\"~settings\" data-link=\"false\" data-link-reference=\"false\" data-group=\"9970\" data-label=\"4107726\" data-reference-type=\"label\" data-container=\"body\" data-placement=\"top\" title=\"Issues related to project, group, profile, or application settings\" class=\"gfm gfm-label has-tooltip gl-link gl-label-link\"><span class=\"gl-label-text gl-label-text-light\" data-container=\"body\" data-html=\"true\" style=\"background-color: #428bca\">settings</span></a></span> that are still relevant. See <a href=\"https://about.gitlab.com/handbook/product/product-management/process/feature-or-bug.html#feature-issues\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">definition of feature issues</a>.</p>&#x000A;<p data-sourcepos=\"5:1-5:101\" dir=\"auto\">This epic is part of the <strong>Settings UX</strong> initiative <a href=\"https://gitlab.com/groups/gitlab-org/-/epics/3535\" data-original=\"https://gitlab.com/groups/gitlab-org/-/epics/3535\" data-link=\"false\" data-link-reference=\"true\" data-group=\"9970\" data-epic=\"64876\" data-reference-type=\"epic\" data-container=\"body\" data-placement=\"top\" title=\"Settings UX\" class=\"gfm gfm-epic has-tooltip\">&amp;3535</a></p>",
              "id": "gid://gitlab/Epic/64883",
              "webUrl": "https://gitlab.com/groups/gitlab-org/-/epics/3537",
              "createdAt": "2020-06-09T13:55:18Z",
              "closedAt": null,
              "state": "opened"
            },
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13861",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Fire Wiki webhooks upon CLI push operations (medium)",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:179\" dir=\"auto\">I need to run some automated job when a wiki page changes. Currently, I see no way to get notifications on wiki page edits. It would be nice if GitLab would support this use case.</p>&#x000A;<p data-sourcepos=\"3:1-3:264\" dir=\"auto\">Additionally, I can't achieve it even with shell access to the GitLab server with setting up native git hooks, since GitLab uses Gollum as wiki engine, Gollum uses Grit for git manipulation, and Grit does not fire git hooks (they manipulate the git repo directly).</p>&#x000A;<p data-sourcepos=\"5:1-5:70\" dir=\"auto\">Please don't hesitate to share any workaround if you are aware of any.</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2020-11-24T18:56:21Z",
            "createdAt": "2014-09-05T08:22:56Z",
            "dueDate": null,
            "upvotes": 26,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/52319",
              "avatarUrl": "https://secure.gravatar.com/avatar/eee96f5b8c3009a51b02318830900fdc?s=80&d=identicon",
              "username": "csirkus"
            },
            "epic": {
              "author": {
                "id": "gid://gitlab/User/1187333",
                "avatarUrl": "/uploads/-/system/user/avatar/1187333/avatar.png",
                "username": "jramsay-gitlab"
              },
              "descriptionHtml": "&#x000A;<p data-sourcepos=\"2:1-4:223\" dir=\"auto\"><em>This page may contain information related to upcoming products, features and functionality.&#x000A;It is important to note that the information presented is for informational purposes only, so please do not rely on the information for purchasing or planning purposes.&#x000A;Just like with all projects, the items mentioned on the page are subject to change or delay, and the development, release, and timing of any products, features, or functionality remain at the sole discretion of GitLab Inc.</em></p>",
              "id": "gid://gitlab/Epic/2961",
              "webUrl": "https://gitlab.com/groups/gitlab-org/-/epics/702",
              "createdAt": "2019-01-10T04:56:32Z",
              "closedAt": null,
              "state": "opened"
            },
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13862",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Large merge request blocks GitLab from deleting it",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:225\" dir=\"auto\">We have a repo where the main branch is not master to emphasize to the devs the branching model. This mean that all branches we create do not originate from master. Additionally, the main branch is kept as \"master\" in gitlab.</p>&#x000A;<p data-sourcepos=\"3:1-3:305\" dir=\"auto\">While this organization is used to force users to think about which branch they are in, it failed us on one point. The default branch is the one used by default during a merge request. We thus ended up with two merge requests to master, which is empty. Selecting the merge request will timeout with a 502:</p>&#x000A;<pre class=\"code highlight js-syntax-highlight language-plaintext\" lang=\"plaintext\" v-pre=\"true\"><code><span id=\"LC1\" class=\"line\" lang=\"plaintext\">502</span>&#x000A;<span id=\"LC2\" class=\"line\" lang=\"plaintext\"></span>&#x000A;<span id=\"LC3\" class=\"line\" lang=\"plaintext\">GitLab is not responding.</span>&#x000A;<span id=\"LC4\" class=\"line\" lang=\"plaintext\"></span>&#x000A;<span id=\"LC5\" class=\"line\" lang=\"plaintext\">Please contact your GitLab administrator if this problem persists.</span></code></pre>&#x000A;<p data-sourcepos=\"11:1-11:48\" dir=\"auto\">since the diff between the two branches is huge.</p>&#x000A;<p data-sourcepos=\"13:1-13:156\" dir=\"auto\">Increasing the GitLab's timeout might allow us delete the bad merge request, but this might not even be sufficient since the diff could still create issues.</p>&#x000A;<p data-sourcepos=\"15:1-15:182\" dir=\"auto\">Is there any other way to delete these bad merge requests without having to go on the command line? The installed instance of GitLab is, let's say it that way, not easily accessible.</p>&#x000A;<p data-sourcepos=\"17:1-17:266\" dir=\"auto\">There should be a \"Close\" button next to a merge request, similarly to the issues list, that would allow closing such merge requests without visiting the actual page. An \"Edit\" button could also be added, also similar to the issues list, to change the merge request.</p>&#x000A;<p data-sourcepos=\"19:1-19:7\" dir=\"auto\">Thanks!</p>&#x000A;<p data-sourcepos=\"21:1-21:67\" dir=\"auto\">Using GitLab 7.3.2 on CentOS 6.5 x86_64, installed through omnibus.</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2021-09-14T14:47:16Z",
            "createdAt": "2014-11-11T17:34:42Z",
            "dueDate": null,
            "upvotes": 2,
            "downvotes": 0,
            "weight": 5,
            "author": {
              "id": "gid://gitlab/User/58637",
              "avatarUrl": "https://secure.gravatar.com/avatar/2ea88a505ffd3145da9e390bd8f02888?s=80&d=identicon",
              "username": "nbigaouette"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13872",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "downloading archive should have posibility to get archive based on project name and the revision requested (with corect content-disposition)",
            "descriptionHtml": "<p data-sourcepos=\"1:1-2:92\" dir=\"auto\">downloading from <a href=\"https://gitserver/mygroup/my-nice-project/repository/archive.tar.gz?ref=v0.0.1#/my-nice-project-0.0.1.tar.gz\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">https://gitserver/mygroup/my-nice-project/repository/archive.tar.gz?ref=v0.0.1#/my-nice-project-0.0.1.tar.gz</a>&#x000A;should result in archive without filename based on the requested reference and not the sha1.</p>&#x000A;<p data-sourcepos=\"4:1-4:145\" dir=\"auto\">in my example - it would be nice to generate my-nice-project-0.0.1.tar.gz and not my-nice-project-9db769e6c3e5318d62f4572f6266fb8cf3de0bb8.tar.gz</p>&#x000A;<p data-sourcepos=\"6:1-6:136\" dir=\"auto\">there are some ways (used for example in rpm packaging ) to give a suggestion how to name the file like the part after \"#\" in my example</p>&#x000A;<p data-sourcepos=\"8:1-8:75\" dir=\"auto\">(there were couple of hacks how to solve similar situations for github too)</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2020-09-02T16:18:51Z",
            "createdAt": "2015-02-13T13:38:40Z",
            "dueDate": null,
            "upvotes": 0,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/96904",
              "avatarUrl": "https://secure.gravatar.com/avatar/2facd0955ddc18c7167b047281e53461?s=80&d=identicon",
              "username": "jhrcz"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13863",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "A comment for Shibboleth.md",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:192\" dir=\"auto\">In doc/integration/shibboleth.md (gitlab-ce@d76c5824bc05640d276be96f7853f2d266fd6750), an example using Apache mod_shib2 is introduced. I think that it is nice for Gitlab &amp; Shibboleth users :)</p>&#x000A;<p data-sourcepos=\"3:1-3:184\" dir=\"auto\">In the example configuration, while 'upper case' attributes, like HTTP_EPPN, are manually appended, it may be easier to use :request_type option supported in omniauth-shibboleth 1.0.8.</p>&#x000A;<pre class=\"code highlight js-syntax-highlight language-plaintext\" lang=\"plaintext\" v-pre=\"true\"><code><span id=\"LC1\" class=\"line\" lang=\"plaintext\">gitlab_rails['omniauth_providers'] = [</span>&#x000A;<span id=\"LC2\" class=\"line\" lang=\"plaintext\">  {</span>&#x000A;<span id=\"LC3\" class=\"line\" lang=\"plaintext\">    \"name\" =&gt; 'shibboleth',</span>&#x000A;<span id=\"LC4\" class=\"line\" lang=\"plaintext\">    \"args\" =&gt; {</span>&#x000A;<span id=\"LC5\" class=\"line\" lang=\"plaintext\">      \"request_type\" =&gt; 'header',</span>&#x000A;<span id=\"LC6\" class=\"line\" lang=\"plaintext\">      \"name_field\" =&gt; 'cn',</span>&#x000A;<span id=\"LC7\" class=\"line\" lang=\"plaintext\">      \"info_fields\" =&gt; { \"email\" =&gt; 'mail'}</span>&#x000A;<span id=\"LC8\" class=\"line\" lang=\"plaintext\">    }</span>&#x000A;<span id=\"LC9\" class=\"line\" lang=\"plaintext\">  }</span>&#x000A;<span id=\"LC10\" class=\"line\" lang=\"plaintext\">]</span></code></pre>&#x000A;<p data-sourcepos=\"16:1-16:314\" dir=\"auto\">Furthermore, another option for gitlab-omnibus package (handling Shibboleth request in Nginx environment) is using <a href=\"https://github.com/toyokazu/rack-saml\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">rack-saml</a> with <a href=\"https://github.com/toyokazu/omniauth-shibboleth\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">omniauth-shibboleth</a>. It may be more easier because it does not require Apache and mod_shib2.</p>&#x000A;<p data-sourcepos=\"18:1-18:129\" dir=\"auto\">I am sorry that I do not have enough time to test them. If someone is interested in them, please test and update Shibboleth.md :)</p>&#x000A;<p data-sourcepos=\"20:1-20:4\" dir=\"auto\">Best</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2020-09-02T16:18:47Z",
            "createdAt": "2015-02-28T06:27:25Z",
            "dueDate": null,
            "upvotes": 0,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/32000",
              "avatarUrl": "https://secure.gravatar.com/avatar/100c55d27cf492cbe1599b9fa9f54eab?s=80&d=identicon",
              "username": "toyokazu"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13864",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Adjust Backup task workflow",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:140\" dir=\"auto\"><strong>Summary:</strong> When running the backup task, if there's an error while uploading to remote storage then the backup dir is getting cluttered up</p>&#x000A;<p data-sourcepos=\"3:1-3:83\" dir=\"auto\"><strong>Steps to reproduce:</strong> run the backup task with an upload job that fails somewhere</p>&#x000A;<p data-sourcepos=\"5:1-5:104\" dir=\"auto\"><strong>Expected behavior:</strong> Backup task finishes up cleanup and remove of old backups before trying to upload</p>&#x000A;<p data-sourcepos=\"7:1-7:205\" dir=\"auto\"><strong>Observed behavior:</strong> Backup task is uploading, then gets an error while uploading, leaving the temp files and the old backups in the folder, resulting in the HDD getting full pretty quick on a daily task</p>&#x000A;<p data-sourcepos=\"9:1-9:37\" dir=\"auto\"><strong>Relevant logs and/or screenshots:</strong></p>&#x000A;<p data-sourcepos=\"11:1-11:97\" dir=\"auto\"><strong>Output of checks:</strong> Not relevant for this as it's not an gitlab error but a simple logic issue.</p>&#x000A;<p data-sourcepos=\"13:1-13:19\" dir=\"auto\"><strong>Possible fixes:</strong></p>&#x000A;<p data-sourcepos=\"15:1-15:178\" dir=\"auto\">Move line <a href=\"https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/backup/manager.rb#L29\">https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/backup/manager.rb#L29</a> after <a href=\"https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/tasks/gitlab/backup.rake#L18\">https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/tasks/gitlab/backup.rake#L18</a></p>&#x000A;<p data-sourcepos=\"17:1-17:168\" dir=\"auto\">Add a simple <code>return tar_file</code> to manager.rb where the line was to catch it on the rake task (or make tar_file a class variable and make upload's argument optional)</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2020-09-02T16:18:44Z",
            "createdAt": "2015-03-05T04:42:45Z",
            "dueDate": null,
            "upvotes": 0,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/8318",
              "avatarUrl": "https://secure.gravatar.com/avatar/85bb875aad34be88aa3c0463b8d8949c?s=80&d=identicon",
              "username": "neico"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13871",
            "moved": false
          },
          {
            "blockedByCount": 4,
            "title": "Wiki editing preview does not render AsciiDoc / RDoc / Org",
            "descriptionHtml": "<h3 data-sourcepos=\"1:1-1:20\" dir=\"auto\">&#x000A;<a id=\"user-content-problem-to-solve\" class=\"anchor\" href=\"#problem-to-solve\" aria-hidden=\"true\"></a>Problem to solve</h3>&#x000A;<p data-sourcepos=\"3:1-3:211\" dir=\"auto\">Selecting a document format other than Markdown in wikis doesn't seem to switch preview rendering engine. After saving the document it is rendered correctly, but there is not a funcioning preview during editing.</p>&#x000A;<h3 data-sourcepos=\"5:1-5:12\" dir=\"auto\">&#x000A;<a id=\"user-content-proposal\" class=\"anchor\" href=\"#proposal\" aria-hidden=\"true\"></a>Proposal</h3>&#x000A;<p data-sourcepos=\"7:1-7:11\" dir=\"auto\">First step:</p>&#x000A;<ul data-sourcepos=\"9:1-10:0\" dir=\"auto\">&#x000A;<li data-sourcepos=\"9:1-10:0\">Disable preview for unsupported formats: <a href=\"https://gitlab.com/gitlab-org/gitlab/-/issues/228883\" data-original=\"https://gitlab.com/gitlab-org/gitlab/-/issues/228883\" data-link=\"false\" data-link-reference=\"true\" data-project=\"278964\" data-issue=\"57283998\" data-reference-type=\"issue\" data-container=\"body\" data-placement=\"top\" title=\"Disable preview for unsupported wiki formats\" class=\"gfm gfm-issue has-tooltip\">#228883</a>&#x000A;</li>&#x000A;</ul>&#x000A;<p data-sourcepos=\"11:1-11:54\" dir=\"auto\">Further iterations: Implement preview for all formats:</p>&#x000A;<ul data-sourcepos=\"13:1-16:0\" dir=\"auto\">&#x000A;<li data-sourcepos=\"13:1-13:64\">AsciiDoc: <a href=\"https://gitlab.com/gitlab-org/gitlab/-/issues/228885\" data-original=\"https://gitlab.com/gitlab-org/gitlab/-/issues/228885\" data-link=\"false\" data-link-reference=\"true\" data-project=\"278964\" data-issue=\"57289847\" data-reference-type=\"issue\" data-container=\"body\" data-placement=\"top\" title=\"Implement preview for AsciiDoc wiki pages\" class=\"gfm gfm-issue has-tooltip\">#228885</a>&#x000A;</li>&#x000A;<li data-sourcepos=\"14:1-14:59\">Org: <a href=\"https://gitlab.com/gitlab-org/gitlab/-/issues/228886\" data-original=\"https://gitlab.com/gitlab-org/gitlab/-/issues/228886\" data-link=\"false\" data-link-reference=\"true\" data-project=\"278964\" data-issue=\"57291220\" data-reference-type=\"issue\" data-container=\"body\" data-placement=\"top\" title=\"Implement preview for Org wiki pages\" class=\"gfm gfm-issue has-tooltip\">#228886</a>&#x000A;</li>&#x000A;<li data-sourcepos=\"15:1-16:0\">RDoc: <a href=\"https://gitlab.com/gitlab-org/gitlab/-/issues/228887\" data-original=\"https://gitlab.com/gitlab-org/gitlab/-/issues/228887\" data-link=\"false\" data-link-reference=\"true\" data-project=\"278964\" data-issue=\"57291257\" data-reference-type=\"issue\" data-container=\"body\" data-placement=\"top\" title=\"Implement preview for RDoc wiki pages\" class=\"gfm gfm-issue has-tooltip\">#228887</a>&#x000A;</li>&#x000A;</ul>&#x000A;<p data-sourcepos=\"17:1-17:111\" dir=\"auto\">The actual work will be tracked in the issues above, and we can close this issue once all of it is implemented.</p>&#x000A;<h3 data-sourcepos=\"19:1-19:9\" dir=\"auto\">&#x000A;<a id=\"user-content-links\" class=\"anchor\" href=\"#links\" aria-hidden=\"true\"></a>Links</h3>&#x000A;<p data-sourcepos=\"21:1-21:144\" dir=\"auto\">There's a previous attempt to implement this at <a href=\"https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/16190\" data-original=\"https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/16190\" data-link=\"false\" data-link-reference=\"true\" data-project=\"13083\" data-merge-request=\"6990216\" data-project-path=\"gitlab-org/gitlab-foss\" data-iid=\"16190\" data-mr-title=\"WIP: fix: added preview support for asciidoc and rdoc formats\" data-reference-type=\"merge_request\" data-container=\"body\" data-placement=\"top\" title=\"\" class=\"gfm gfm-merge_request\">gitlab-foss!16190 (closed)</a>, if possible we can reuse that.</p>&#x000A;<p data-sourcepos=\"23:1-23:143\" dir=\"auto\">Most of the changes are around making the markup format generic, so once that's in place adding more formats won't take much additional effort.</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2021-07-19T02:26:16Z",
            "createdAt": "2015-03-10T15:17:11Z",
            "dueDate": null,
            "upvotes": 27,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/109151",
              "avatarUrl": "https://secure.gravatar.com/avatar/757ba4000783666f894895a8761d4d9b?s=80&d=identicon",
              "username": "Reinis"
            },
            "epic": {
              "author": {
                "id": "gid://gitlab/User/1187333",
                "avatarUrl": "/uploads/-/system/user/avatar/1187333/avatar.png",
                "username": "jramsay-gitlab"
              },
              "descriptionHtml": "&#x000A;<p data-sourcepos=\"2:1-4:223\" dir=\"auto\"><em>This page may contain information related to upcoming products, features and functionality.&#x000A;It is important to note that the information presented is for informational purposes only, so please do not rely on the information for purchasing or planning purposes.&#x000A;Just like with all projects, the items mentioned on the page are subject to change or delay, and the development, release, and timing of any products, features, or functionality remain at the sole discretion of GitLab Inc.</em></p>",
              "id": "gid://gitlab/Epic/2960",
              "webUrl": "https://gitlab.com/groups/gitlab-org/-/epics/701",
              "createdAt": "2019-01-10T04:48:42Z",
              "closedAt": null,
              "state": "opened"
            },
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13873",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "google bot shows a memory leak in the gitlab worker",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:19\" dir=\"auto\">Steps to reproduce:</p>&#x000A;<ul data-sourcepos=\"3:1-7:0\" dir=\"auto\">&#x000A;<li data-sourcepos=\"3:1-3:57\">create a project and mirror <a href=\"http://github.com/ceph/ceph\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">http://github.com/ceph/ceph</a>&#x000A;</li>&#x000A;<li data-sourcepos=\"4:1-4:95\">use <a href=\"https://pypi.python.org/pypi/github2gitlab\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">https://pypi.python.org/pypi/github2gitlab</a> to import the ~4,000 pull requests as branches</li>&#x000A;<li data-sourcepos=\"5:1-5:38\">wait for the google crawler to visit</li>&#x000A;<li data-sourcepos=\"6:1-7:0\">watch the worker use all the available memory until it gets killed by the limit set in config.ru (tried with 2GB instead of the default 250MB: it takes about half an hour to get to to 2GB)</li>&#x000A;</ul>&#x000A;<p data-sourcepos=\"8:1-8:12\" dir=\"auto\">GitLab 7.5.1</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2019-11-06T04:22:13Z",
            "createdAt": "2015-03-25T00:49:02Z",
            "dueDate": null,
            "upvotes": 0,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/71978",
              "avatarUrl": "https://secure.gravatar.com/avatar/131beae824c4866ed6ae995c7fc9bd58?s=80&d=identicon",
              "username": "dachary"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13879",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Groups should be public if they have projects shared with them.",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:186\" dir=\"auto\">This is in continuation of <a href=\"http://stackoverflow.com/questions/29200998/are-gitlab-groups-never-public\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">http://stackoverflow.com/questions/29200998/are-gitlab-groups-never-public</a> and <a href=\"https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/451\">merge request 451</a>.</p>&#x000A;<p data-sourcepos=\"3:1-3:98\" dir=\"auto\">It would be nice to be able to make a group public if some repositories are shared with the group.</p>&#x000A;<p data-sourcepos=\"5:1-5:201\" dir=\"auto\">For example, there is no official git repository of NetBSD; however, a single NetBSD developer (currently \"jsonn\" on GitHub) may volunteer to create one under his own account, and share it with NetBSD.</p>&#x000A;<p data-sourcepos=\"7:1-7:296\" dir=\"auto\">Similarly, I'd like to share my <a href=\"http://mdoc.su/\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">http://mdoc.su/</a> with the NetBSD group (as well as OpenBSD, DragonFlyBSD etc).  Sharing is already possible on GitLab, but it would seem weird to have to create a public project for such sharing itself to be public (also, is the sharing public in the first place?).</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2019-11-06T04:22:13Z",
            "createdAt": "2015-03-25T16:50:36Z",
            "dueDate": null,
            "upvotes": 0,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/105839",
              "avatarUrl": "https://secure.gravatar.com/avatar/1ab0e19086582d27abe75b0635f57f43?s=80&d=identicon",
              "username": "cnst"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13881",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Add a better error message when /hooks folder is missing in a repository",
            "descriptionHtml": "<h2 data-sourcepos=\"1:1-1:13\" dir=\"auto\">&#x000A;<a id=\"user-content-background\" class=\"anchor\" href=\"#background\" aria-hidden=\"true\"></a>Background</h2>&#x000A;<p data-sourcepos=\"2:1-2:180\" dir=\"auto\">My repositories are stored on a mounted NFS share, the path is correct in gitlab.yml and everything works 100% correctly. Until someone (outside of my control) moves the NFS share.</p>&#x000A;<p data-sourcepos=\"4:1-4:182\" dir=\"auto\">For some reason the /hooks folder in nearly all the repositories disappeared when the share was moved. This probably has to do with the vagaries of symlink's in linux. But I digress.</p>&#x000A;<h2 data-sourcepos=\"6:1-6:17\" dir=\"auto\">&#x000A;<a id=\"user-content-actual-problem\" class=\"anchor\" href=\"#actual-problem\" aria-hidden=\"true\"></a>Actual Problem</h2>&#x000A;<p data-sourcepos=\"7:1-7:164\" dir=\"auto\">None of the repos were reporting updates on the Dashboard, but I could see that commit's were happening when checking individual repos. A quick check on the server:</p>&#x000A;<pre class=\"code highlight js-syntax-highlight language-plaintext\" lang=\"plaintext\" v-pre=\"true\"><code><span id=\"LC1\" class=\"line\" lang=\"plaintext\">sudo -u git -H bundle exec rake gitlab:check RAILS_ENV=production</span></code></pre>&#x000A;<p data-sourcepos=\"11:1-11:9\" dir=\"auto\">Revealed:</p>&#x000A;<pre class=\"code highlight js-syntax-highlight language-plaintext\" lang=\"plaintext\" v-pre=\"true\"><code><span id=\"LC1\" class=\"line\" lang=\"plaintext\">Errno::ENOENT: No such file or directory @ realpath_rec - /mnt/repos/group/example.git/hooks</span>&#x000A;<span id=\"LC2\" class=\"line\" lang=\"plaintext\">/home/git/gitlab/lib/tasks/gitlab/check.rake:509:in `realpath'</span>&#x000A;<span id=\"LC3\" class=\"line\" lang=\"plaintext\">/home/git/gitlab/lib/tasks/gitlab/check.rake:509:in `block in check_repos_hooks_directory_is_link'</span>&#x000A;<span id=\"LC4\" class=\"line\" lang=\"plaintext\">/home/git/gitlab/vendor/bundle/ruby/2.1.0/gems/activerecord-4.1.9/lib/active_record/relation/batches.rb:52:in `block (2 levels) in find_each'</span>&#x000A;<span id=\"LC5\" class=\"line\" lang=\"plaintext\">/home/git/gitlab/vendor/bundle/ruby/2.1.0/gems/activerecord-4.1.9/lib/active_record/relation/batches.rb:52:in `each'</span>&#x000A;<span id=\"LC6\" class=\"line\" lang=\"plaintext\">/home/git/gitlab/vendor/bundle/ruby/2.1.0/gems/activerecord-4.1.9/lib/active_record/relation/batches.rb:52:in `block in find_each'</span>&#x000A;<span id=\"LC7\" class=\"line\" lang=\"plaintext\">/home/git/gitlab/vendor/bundle/ruby/2.1.0/gems/activerecord-4.1.9/lib/active_record/relation/batches.rb:125:in `find_in_batches'</span>&#x000A;<span id=\"LC8\" class=\"line\" lang=\"plaintext\">/home/git/gitlab/vendor/bundle/ruby/2.1.0/gems/activerecord-4.1.9/lib/active_record/relation/batches.rb:51:in `find_each'</span>&#x000A;<span id=\"LC9\" class=\"line\" lang=\"plaintext\">/home/git/gitlab/vendor/bundle/ruby/2.1.0/gems/activerecord-4.1.9/lib/active_record/querying.rb:9:in `find_each'</span>&#x000A;<span id=\"LC10\" class=\"line\" lang=\"plaintext\">/home/git/gitlab/lib/tasks/gitlab/check.rake:503:in `check_repos_hooks_directory_is_link'</span>&#x000A;<span id=\"LC11\" class=\"line\" lang=\"plaintext\">/home/git/gitlab/lib/tasks/gitlab/check.rake:364:in `block (3 levels) in &lt;top (required)&gt;'</span>&#x000A;<span id=\"LC12\" class=\"line\" lang=\"plaintext\">Tasks: TOP =&gt; gitlab:check =&gt; gitlab:gitlab_shell:check</span>&#x000A;<span id=\"LC13\" class=\"line\" lang=\"plaintext\">(See full trace by running task with --trace)</span>&#x000A;<span id=\"LC14\" class=\"line\" lang=\"plaintext\">stuart@server:/home/git/gitlab$</span></code></pre>&#x000A;<h2 data-sourcepos=\"30:1-30:13\" dir=\"auto\">&#x000A;<a id=\"user-content-manual-fix\" class=\"anchor\" href=\"#manual-fix\" aria-hidden=\"true\"></a>Manual fix</h2>&#x000A;<p data-sourcepos=\"31:1-31:73\" dir=\"auto\">To fix it I had to create the /hooks folder in (nearly all) of the repos:</p>&#x000A;<pre class=\"code highlight js-syntax-highlight language-plaintext\" lang=\"plaintext\" v-pre=\"true\"><code><span id=\"LC1\" class=\"line\" lang=\"plaintext\">sudo -u git -H mkdir /mnt/repos/group/example.git/hooks</span></code></pre>&#x000A;<p data-sourcepos=\"35:1-35:28\" dir=\"auto\">And then run ./create-hooks:</p>&#x000A;<pre class=\"code highlight js-syntax-highlight language-plaintext\" lang=\"plaintext\" v-pre=\"true\"><code><span id=\"LC1\" class=\"line\" lang=\"plaintext\">sudo -u git -H /home/git/gitlab-shell//bin/create-hooks</span></code></pre>&#x000A;<h2 data-sourcepos=\"39:1-39:31\" dir=\"auto\">&#x000A;<a id=\"user-content-suggestion-to-improve-gitlab\" class=\"anchor\" href=\"#suggestion-to-improve-gitlab\" aria-hidden=\"true\"></a>Suggestion to improve Gitlab</h2>&#x000A;<p data-sourcepos=\"40:1-40:216\" dir=\"auto\">I think that if the <code>rake gitlab:check</code> command can detect that a repo has wrong or missing hooks, then it should also detect that the actual <code>/hooks</code> folder is missing, and offer a suggestion on how it can be fixed.</p>&#x000A;<p data-sourcepos=\"42:1-42:112\" dir=\"auto\">Similarly, would <code>./create-hooks</code> not also be capable of actually creating the <code>/hooks</code> folder if it is missing?</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2020-09-02T16:18:32Z",
            "createdAt": "2015-03-27T09:04:19Z",
            "dueDate": null,
            "upvotes": 1,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/120990",
              "avatarUrl": "https://secure.gravatar.com/avatar/77b10422593a1c0b9b170f4513fb0c0c?s=80&d=identicon",
              "username": "sgrassie"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13878",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Version check doesnt handle none git installations",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:49\" dir=\"auto\">We are packaging git as rpm ourself for opensuse.</p>&#x000A;<p data-sourcepos=\"3:1-4:88\" dir=\"auto\">In the help screen we get:&#x000A;\"GitLab 7.10.0.pre fatal: Not a git repository (or any of the parent directories): .git\"</p>&#x000A;<p data-sourcepos=\"6:1-6:74\" dir=\"auto\">Is there any file we can provide which can hold the git revision for this?</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2020-09-01T14:43:48Z",
            "createdAt": "2015-04-10T19:20:34Z",
            "dueDate": null,
            "upvotes": 0,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/123015",
              "avatarUrl": "https://secure.gravatar.com/avatar/eec0b791c6aab72d99940855fe374fda?s=80&d=identicon",
              "username": "darix"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13870",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Version Check image alternative text is useless",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:85\" dir=\"auto\">On /help, the version check image has an alternative text (<code>alt</code>) that reads \"Check\".</p>&#x000A;<p data-sourcepos=\"3:1-3:197\" dir=\"auto\">This text should be the same as what the image reads for accessibility reasons. Screen readers will merely read \"Check\". Poor sighted users who disable images will have this text displayed to them.</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2019-11-06T04:22:12Z",
            "createdAt": "2015-05-23T17:03:13Z",
            "dueDate": null,
            "upvotes": 0,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/64038",
              "avatarUrl": "https://secure.gravatar.com/avatar/c7826f5b297baebddbd568f4b69a2d98?s=80&d=identicon",
              "username": "WhyNotHugo"
            },
            "epic": {
              "author": {
                "id": "gid://gitlab/User/1296187",
                "avatarUrl": "/uploads/-/system/user/avatar/1296187/avatar.png",
                "username": "sarrahvesselov"
              },
              "descriptionHtml": "<p data-sourcepos=\"1:1-1:62\" dir=\"auto\"><a href=\"https://www.w3.org/TR/UNDERSTANDING-WCAG20/text-equiv-all.html\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">https://www.w3.org/TR/UNDERSTANDING-WCAG20/text-equiv-all.html</a></p>&#x000A;<h1 data-sourcepos=\"3:1-3:24\" dir=\"auto\">&#x000A;<a id=\"user-content-111-non-text-content\" class=\"anchor\" href=\"#111-non-text-content\" aria-hidden=\"true\"></a>1.1.1 Non-text Content</h1>&#x000A;<p data-sourcepos=\"5:1-5:159\" dir=\"auto\">All non-text content that is presented to the user has a text alternative that serves the equivalent purpose, except for the situations listed below. (Level A)</p>&#x000A;<p data-sourcepos=\"7:1-7:112\" dir=\"auto\">Link to documentation: <a href=\"https://www.w3.org/TR/UNDERSTANDING-WCAG20/content-structure-separation-programmatic.html\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">https://www.w3.org/TR/UNDERSTANDING-WCAG20/content-structure-separation-programmatic.html</a></p>&#x000A;<p data-sourcepos=\"9:1-9:134\" dir=\"auto\">Link to how to meet these standards: <a href=\"https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#qr-content-structure-separation-programmatic\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#qr-content-structure-separation-programmatic</a></p>&#x000A;<p data-sourcepos=\"11:1-11:226\" dir=\"auto\"><strong>Controls, Input:</strong> If non-text content is a control or accepts user input, then it has a name that describes its purpose. (Refer to Guideline 4.1 for additional requirements for controls and content that accepts user input.)</p>&#x000A;<p data-sourcepos=\"13:1-13:218\" dir=\"auto\"><strong>Time-Based Media:</strong> If non-text content is time-based media, then text alternatives at least provide descriptive identification of the non-text content. (Refer to Guideline 1.2 for additional requirements for media.)</p>&#x000A;<p data-sourcepos=\"15:1-15:187\" dir=\"auto\"><strong>Test:</strong> If non-text content is a test or exercise that would be invalid if presented in text, then text alternatives at least provide descriptive identification of the non-text content.</p>&#x000A;<p data-sourcepos=\"17:1-17:187\" dir=\"auto\"><strong>Sensory:</strong> If non-text content is primarily intended to create a specific sensory experience, then text alternatives at least provide descriptive identification of the non-text content.</p>&#x000A;<p data-sourcepos=\"19:1-19:374\" dir=\"auto\"><strong>CAPTCHA:</strong> If the purpose of non-text content is to confirm that content is being accessed by a person rather than a computer, then text alternatives that identify and describe the purpose of the non-text content are provided, and alternative forms of CAPTCHA using output modes for different types of sensory perception are provided to accommodate different disabilities.</p>&#x000A;<p data-sourcepos=\"21:1-21:224\" dir=\"auto\"><strong>Decoration, Formatting, Invisible:</strong> If non-text content is pure decoration, is used only for visual formatting, or is not presented to users, then it is implemented in a way that it can be ignored by assistive technology.</p>",
              "id": "gid://gitlab/Epic/2181",
              "webUrl": "https://gitlab.com/groups/gitlab-org/-/epics/564",
              "createdAt": "2018-11-28T17:50:23Z",
              "closedAt": null,
              "state": "opened"
            },
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13922",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Auto login using omniauth provider",
            "descriptionHtml": "<p data-sourcepos=\"1:1-2:150\" dir=\"auto\">Is there any way to make auto login possible?&#x000A;Right now I can redirect unauthorized users to some url, but I can't redirect them to /users/auth/PROVIDER because it must be a POST request, not GET.</p>&#x000A;<p data-sourcepos=\"4:1-4:15\" dir=\"auto\">I see two ways:</p>&#x000A;<ol data-sourcepos=\"5:1-7:0\" dir=\"auto\">&#x000A;<li data-sourcepos=\"5:1-5:126\">It may be an option on system settings page like \"Enable auto login through provider: \" and select box with auth providers.</li>&#x000A;<li data-sourcepos=\"6:1-7:0\">It may be a check box after setting 'Home page url' on system settings page like 'do POST redirect'.</li>&#x000A;</ol>&#x000A;<p data-sourcepos=\"8:1-8:48\" dir=\"auto\">It would be great if you could add this feature!</p>&#x000A;<p data-sourcepos=\"10:1-10:40\" dir=\"auto\">Can I hope to see this feature realized?</p>&#x000A;<p data-sourcepos=\"12:1-12:28\" dir=\"auto\">P.S. Sorry for my english =)</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2020-09-02T16:18:12Z",
            "createdAt": "2015-06-04T01:50:16Z",
            "dueDate": null,
            "upvotes": 1,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/59852",
              "avatarUrl": "https://secure.gravatar.com/avatar/4971cd17e16cf6b3bf291c1a52b0f470?s=80&d=identicon",
              "username": "xornet-sl"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/14068",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Show commit message in issues",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:210\" dir=\"auto\">Commits with issue mentions (<code>issue #123456</code>) are displayed in the regarding issue like <code>mentioned in commit 48d55b10</code>. It would be much more helpful if the commit message would be displayed too just like that:</p>&#x000A;<p data-sourcepos=\"3:1-3:100\" dir=\"auto\"><a class=\"no-attachment-icon\" href=\"https://gitlab.com/gitlab-org/gitlab-ce/uploads/201db02109f31ba969ce9aa799f95838/image.png\" target=\"_blank\" rel=\"noopener noreferrer\"><img src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" alt=\"image\" class=\"lazy\" data-src=\"https://gitlab.com/gitlab-org/gitlab-ce/uploads/201db02109f31ba969ce9aa799f95838/image.png\"></a></p>&#x000A;<p data-sourcepos=\"5:1-5:117\" dir=\"auto\">Since the commit messages contain always important information and documentation, which may be relevant to the issue.</p>&#x000A;<p data-sourcepos=\"7:1-7:91\" dir=\"auto\">(I didn't post it to the feature request board, since I think it shouldn't be a big change)</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2020-09-02T16:18:07Z",
            "createdAt": "2015-06-10T11:09:14Z",
            "dueDate": null,
            "upvotes": 33,
            "downvotes": 0,
            "weight": 3,
            "author": {
              "id": "gid://gitlab/User/23522",
              "avatarUrl": "/uploads/-/system/user/avatar/23522/avatar.png",
              "username": "phortx"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/14086",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Update user info with claims provided by SAML SSO every log in",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:30\" dir=\"auto\">Currently this doesn't happen.</p>&#x000A;<p data-sourcepos=\"3:1-3:115\" dir=\"auto\">I.e. log in to gitlab via saml sso --&gt; new user created.  User display name set to \"Foo Bar\" from active directory.</p>&#x000A;<p data-sourcepos=\"5:1-5:74\" dir=\"auto\">\"Foo Bar\" gets married... changes name to \"Foo Smith\" in active directory.</p>&#x000A;<p data-sourcepos=\"7:1-7:145\" dir=\"auto\">Log in to gitlab via saml sso --&gt; user already created.  User display name is \"Foo Bar\".  Would except user display name to be set to \"Foo Smith\"</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2020-09-02T16:18:03Z",
            "createdAt": "2015-06-25T20:09:33Z",
            "dueDate": null,
            "upvotes": 0,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/163631",
              "avatarUrl": "https://secure.gravatar.com/avatar/958741385e2fb41a350d041527ec5c90?s=80&d=identicon",
              "username": "wgordonw1"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13882",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Add support for OpenID",
            "descriptionHtml": "<p data-sourcepos=\"1:1-2:73\" dir=\"auto\">The internet is a wonderful place, where nobody needs an passport to walk on.&#x000A;Why not allowing people to log into gitlab with self-certified identity ?</p>&#x000A;<p data-sourcepos=\"4:1-4:177\" dir=\"auto\">OpenID is well supported, has many providers and I guess you're already using it to allow logging in with accounts from major corporations. How about also being nice to people ?</p>&#x000A;<p data-sourcepos=\"6:1-6:36\" dir=\"auto\"><a href=\"https://en.wikipedia.org/wiki/OpenID\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">https://en.wikipedia.org/wiki/OpenID</a></p>",
            "totalTimeSpent": 0,
            "updatedAt": "2020-12-02T14:57:16Z",
            "createdAt": "2015-07-08T17:24:59Z",
            "dueDate": null,
            "upvotes": 5,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/146204",
              "avatarUrl": "https://secure.gravatar.com/avatar/fe2a9e759730ee64c44bf8901bf4ccc3?s=80&d=identicon",
              "username": "strk"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13926",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Support straight diff between commits with CompareService",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:335\" dir=\"auto\">Right now, the CompareService does the equivalent of <code>git diff commita...commitb</code>. It would be nice to have a way to as for <code>git diff commita commitb</code>. We would like to have a page to link to which shows the diff between updates to a merge request (currently added by our bot, but it also might fit somewhere on the MR comment itself).</p>&#x000A;<p data-sourcepos=\"3:1-3:7\" dir=\"auto\"><a href=\"/stanhu\" data-user=\"64248\" data-reference-type=\"user\" data-container=\"body\" data-placement=\"top\" class=\"gfm gfm-project_member js-user-link\" title=\"Stan Hu\">@stanhu</a></p>",
            "totalTimeSpent": 0,
            "updatedAt": "2021-03-03T00:08:28Z",
            "createdAt": "2015-07-09T17:47:43Z",
            "dueDate": null,
            "upvotes": 8,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/152826",
              "avatarUrl": "https://secure.gravatar.com/avatar/34b66417659a2cfb5099121ae5fc16d1?s=80&d=identicon",
              "username": "ben.boeckel"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13930",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Add GitLab icon in Slack payload",
            "descriptionHtml": "<p data-sourcepos=\"1:1-3:63\" dir=\"auto\">In the Slack API there is possibility to add the icon to payload JSON (see <a href=\"https://api.slack.com/incoming-webhooks\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">https://api.slack.com/incoming-webhooks</a>).&#x000A;This will allow to distinguish the bot which uses the Slack API endpoint (since it could be used by different apps) from others.&#x000A;Could you please add this option to the Slack service template?</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2020-08-14T17:44:24Z",
            "createdAt": "2015-07-14T08:49:37Z",
            "dueDate": null,
            "upvotes": 10,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/191908",
              "avatarUrl": "https://secure.gravatar.com/avatar/22361107f1697a84adb776108b5df4c7?s=80&d=identicon",
              "username": "aberdyshev"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13883",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "UX Improvement for Public Deploy Keys",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:317\" dir=\"auto\">If I view the deploy keys for in the project settings page, I also see the other projects, where this key is enabled.I think this information is not necessary on that view. I just want to know which keys are enabled for that project. Omitting that information would also lead to better performance for page load time.</p>&#x000A;<p data-sourcepos=\"3:1-3:326\" dir=\"auto\">The information where a certain public deploy key is installed would make more sense in the public deploy keys page, under the admin section. If I click the link for a key I would get the key information, and also which project have that key enabled. One could add a enable/disable button for each project to disable that key.</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2021-01-08T16:26:13Z",
            "createdAt": "2015-07-16T06:50:18Z",
            "dueDate": null,
            "upvotes": 1,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/39170",
              "avatarUrl": "https://secure.gravatar.com/avatar/356268951446dc675130268e35da131d?s=80&d=identicon",
              "username": "zbindenren"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13884",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Project Dashboard - Add VERSION information to top/static section of project dashboard",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:113\" dir=\"auto\">In release 7.13 the dashboard redesign removed some automated functionality that is considered quite key by some:</p>&#x000A;<ul data-sourcepos=\"3:1-5:0\" dir=\"auto\">&#x000A;<li data-sourcepos=\"3:1-3:188\">CI status badge on dashboard. Currently this can be shown if you use a <code>readme.md</code> on the dashboard but as a manager I want to see the contribution history and the CI status on one page.</li>&#x000A;<li data-sourcepos=\"4:1-5:0\">Version (taken from a projects <code>VERSION</code> file of the primary branch)</li>&#x000A;</ul>&#x000A;<p data-sourcepos=\"6:1-6:175\" dir=\"auto\">While I'd love to see both of these come back, the more important one for me is VERSION simply because there is no other way of displaying (can't be hooked in the <code>readme.md</code>)</p>&#x000A;<h2 data-sourcepos=\"8:1-8:11\" dir=\"auto\">&#x000A;<a id=\"user-content-solution\" class=\"anchor\" href=\"#solution\" aria-hidden=\"true\"></a>Solution</h2>&#x000A;<p data-sourcepos=\"10:1-10:229\" dir=\"auto\">Add version (if present) to the lower set of links on profile. I'd suggest putting it to the left of Changelog. To preserve the aesthetic of the new UI it has been suggested that <code>Contribution guide</code> be renamed to <code>Contributing</code>.</p>&#x000A;<blockquote data-sourcepos=\"12:1-12:114\" dir=\"auto\">&#x000A;<p data-sourcepos=\"12:3-12:114\">Suggested by Sytse Sijbrandij here: <a href=\"https://about.gitlab.com/2015/07/22/gitlab-7-13-released/#comment-2152739360\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">https://about.gitlab.com/2015/07/22/gitlab-7-13-released/#comment-2152739360</a></p>&#x000A;</blockquote>&#x000A;<p data-sourcepos=\"14:1-14:100\" dir=\"auto\"><a class=\"no-attachment-icon\" href=\"https://gitlab.com/gitlab-org/gitlab-ce/uploads/4588f98370151dea5274012bb395e172/image.png\" target=\"_blank\" rel=\"noopener noreferrer\"><img src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" alt=\"image\" class=\"lazy\" data-src=\"https://gitlab.com/gitlab-org/gitlab-ce/uploads/4588f98370151dea5274012bb395e172/image.png\"></a></p>",
            "totalTimeSpent": 0,
            "updatedAt": "2021-09-15T04:41:17Z",
            "createdAt": "2015-07-23T14:20:12Z",
            "dueDate": null,
            "upvotes": 7,
            "downvotes": 0,
            "weight": 3,
            "author": {
              "id": "gid://gitlab/User/199739",
              "avatarUrl": "https://secure.gravatar.com/avatar/9d3a7bd1a1ca320137af912e97621eb1?s=80&d=identicon",
              "username": "madhancock"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13889",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "LDAP and First letter from local charset",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:3\" dir=\"auto\">Hi,</p>&#x000A;<p data-sourcepos=\"3:1-3:169\" dir=\"auto\">I have found very annoying thing about clone repo via gitlab when gl is connected to LDAP/AD and Name or Surname of the user starts with character from local charset ie:</p>&#x000A;<p data-sourcepos=\"5:1-5:36\" dir=\"auto\">CN=Jan Nowak,OU=Devs,OU=Users,......</p>&#x000A;<p data-sourcepos=\"7:1-7:35\" dir=\"auto\">is able to logon and clone repo BUT</p>&#x000A;<p data-sourcepos=\"9:1-9:40\" dir=\"auto\">CN=Łukasz Nowak,OU=Devs,OU=Users,......</p>&#x000A;<p data-sourcepos=\"11:1-11:67\" dir=\"auto\">can logon to gitlab but any try of clonning repo results 401 error:</p>&#x000A;<p data-sourcepos=\"13:1-13:58\" dir=\"auto\">fatal: Authentication failed for '<a href=\"https://lukaszl\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">https://lukaszl</a>.........</p>&#x000A;<p data-sourcepos=\"16:1-18:17\" dir=\"auto\">Recreating domain account to:&#x000A;CN=Lukasz Nowak,OU=Devs,OU=Users,......&#x000A;solve problem...</p>&#x000A;<p data-sourcepos=\"20:1-20:31\" dir=\"auto\">Can You fix that strange thing?</p>&#x000A;<p data-sourcepos=\"22:1-22:49\" dir=\"auto\">This \"feature\" exists in version 7.0.0 and 7.13.2</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2020-09-02T16:17:54Z",
            "createdAt": "2015-07-29T13:21:09Z",
            "dueDate": null,
            "upvotes": 0,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/23210",
              "avatarUrl": "https://secure.gravatar.com/avatar/4bb006d217a9e0428fa788469f8dc616?s=80&d=identicon",
              "username": "Replay1111"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13928",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Unable to update primary email",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:8\" dir=\"auto\">Hi Team,</p>&#x000A;<p data-sourcepos=\"3:1-6:12\" dir=\"auto\">I am using LDAP to login into Gitlab version 7.9.1&#x000A;By default the primary email id is created with the username i.e \"<a href=\"mailto:username@d2-bfg.com\">username@d2-bfg.com</a>\".&#x000A;I want to update the primary email id with the valid email id.&#x000A;Please help.</p>&#x000A;<p data-sourcepos=\"8:1-9:5\" dir=\"auto\">Thanks and regards,&#x000A;Kawal</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2020-09-02T16:17:53Z",
            "createdAt": "2015-07-30T19:18:00Z",
            "dueDate": null,
            "upvotes": 0,
            "downvotes": 1,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/205058",
              "avatarUrl": "https://secure.gravatar.com/avatar/c26c277dcaa7d3f4909609207be4ace9?s=80&d=identicon",
              "username": "kawal"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13886",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Ability names are confusing",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:4\" dir=\"auto\">See:</p>&#x000A;<ul data-sourcepos=\"2:1-4:0\" dir=\"auto\">&#x000A;<li data-sourcepos=\"2:1-2:74\"><a href=\"https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/1037#note_1707920\">https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/1037#note_1707920</a></li>&#x000A;<li data-sourcepos=\"3:1-4:0\"><a href=\"https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/1059#note_1721986\">https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/1059#note_1721986</a></li>&#x000A;</ul>&#x000A;<p data-sourcepos=\"5:1-5:295\" dir=\"auto\">It's really hard to keep track of the difference between <code>admin_*</code> and <code>update_*</code>, and we've got inconsistencies, where <code>:admin_project_member</code> and <code>:admin_group_member</code> need the project/group as their object, while <code>:update_group_member</code> and <code>:destroy_group_member</code> need the actual GroupMember.</p>&#x000A;<p data-sourcepos=\"7:1-7:28\" dir=\"auto\">cc <a href=\"/rspeicher\" data-user=\"15139\" data-reference-type=\"user\" data-container=\"body\" data-placement=\"top\" class=\"gfm gfm-project_member js-user-link\" title=\"Robert Speicher\">@rspeicher</a> <a href=\"/dzaporozhets\" data-user=\"444\" data-reference-type=\"user\" data-container=\"body\" data-placement=\"top\" class=\"gfm gfm-project_member js-user-link\" title=\"Dmitriy Zaporozhets (DZ)\">@dzaporozhets</a></p>",
            "totalTimeSpent": 0,
            "updatedAt": "2019-11-06T04:22:10Z",
            "createdAt": "2015-07-31T12:19:18Z",
            "dueDate": null,
            "upvotes": 0,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/87854",
              "avatarUrl": "https://secure.gravatar.com/avatar/73d1548ae618321220679b3a4fda7fb1?s=80&d=identicon",
              "username": "DouweM"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13888",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Custom avatars are not cached by the browser",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:105\" dir=\"auto\">As of Gitlab CE 7.13.2 custom Avatars are not cached in recent versions of (at least) Firefox and Chrome.</p>&#x000A;<p data-sourcepos=\"3:1-3:189\" dir=\"auto\">If I upload a custom avatar, the avatar picture at the bottom left is re-requested and retransferred each time the Gitlab web is loaded. I would have expected it to be cached for some time.</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2019-11-06T04:22:09Z",
            "createdAt": "2015-08-03T15:00:38Z",
            "dueDate": null,
            "upvotes": 2,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/207440",
              "avatarUrl": "/uploads/-/system/user/avatar/207440/avatar.png",
              "username": "thewilli"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13923",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Linking SAML account with LDAP account automatically fails",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:452\" dir=\"auto\">I'm using CentOS 6.6 and gitlab-omnibus Community Edition from the CentOS yum repository version 7.12.2. I have successfully implemented LDAP auth and SAML auth using ominauth. Initially all users logged in and created their accounts through ldap. Now I want them to be able to log in over SAML. The configuration parameter <code>omniauth_auto_link_ldap_user</code> doesn't link accounts coming in through saml to accounts that were previously created using LDAP.</p>&#x000A;<p data-sourcepos=\"3:1-3:38\" dir=\"auto\">Here is my gitlab.rb entries for SAML:</p>&#x000A;<pre class=\"code highlight js-syntax-highlight language-plaintext\" lang=\"plaintext\" v-pre=\"true\"><code><span id=\"LC1\" class=\"line\" lang=\"plaintext\">gitlab_rails['omniauth_enabled'] = true</span>&#x000A;<span id=\"LC2\" class=\"line\" lang=\"plaintext\">gitlab_rails['omniauth_allow_single_sign_on'] = true</span>&#x000A;<span id=\"LC3\" class=\"line\" lang=\"plaintext\">gitlab_rails['omniauth_auto_sign_in_with_provider'] = 'saml'</span>&#x000A;<span id=\"LC4\" class=\"line\" lang=\"plaintext\">gitlab_rails['omniauth_block_auto_created_users'] = false</span>&#x000A;<span id=\"LC5\" class=\"line\" lang=\"plaintext\">gitlab_rails['omniauth_auto_link_ldap_user'] = true</span>&#x000A;<span id=\"LC6\" class=\"line\" lang=\"plaintext\"></span>&#x000A;<span id=\"LC7\" class=\"line\" lang=\"plaintext\">gitlab_rails['omniauth_providers'] = [</span>&#x000A;<span id=\"LC8\" class=\"line\" lang=\"plaintext\">  {</span>&#x000A;<span id=\"LC9\" class=\"line\" lang=\"plaintext\">    \"name\" =&gt; \"saml\",</span>&#x000A;<span id=\"LC10\" class=\"line\" lang=\"plaintext\">     args: {</span>&#x000A;<span id=\"LC11\" class=\"line\" lang=\"plaintext\">             issuer: 'https://gitlab.domain.com',</span>&#x000A;<span id=\"LC12\" class=\"line\" lang=\"plaintext\">             assertion_consumer_service_url: 'https://gitlab.domain.com/users/auth/saml/callback',</span>&#x000A;<span id=\"LC13\" class=\"line\" lang=\"plaintext\">             idp_cert_fingerprint: '####################',</span>&#x000A;<span id=\"LC14\" class=\"line\" lang=\"plaintext\">             idp_sso_target_url: 'https://sso.domain.com/adfs/ls/',</span>&#x000A;<span id=\"LC15\" class=\"line\" lang=\"plaintext\">             name_identifier_format: 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient'</span>&#x000A;<span id=\"LC16\" class=\"line\" lang=\"plaintext\">           }</span>&#x000A;<span id=\"LC17\" class=\"line\" lang=\"plaintext\">  }</span>&#x000A;<span id=\"LC18\" class=\"line\" lang=\"plaintext\">]</span></code></pre>&#x000A;<p data-sourcepos=\"25:1-25:224\" dir=\"auto\">If I disable <code>omniauth_auto_sign_in_with_provider</code>, then users can log in over ldap, go to their profile, then accounts settings and manually link the two accounts together. I want gitlab to automatically link them together.</p>&#x000A;<p data-sourcepos=\"27:1-27:304\" dir=\"auto\">I see that <code>omniauth_auto_sign_in_with_provider</code> uses a field called <code>extern_uid</code> to verify if they two users should be linked. The issue is that when logging in over ldap, <code>extern_uid</code> becomes the distinguished name(dn) and when logging in over Omni-Auth SAML the <code>extern_uid</code> becomes the email address.</p>&#x000A;<p data-sourcepos=\"29:1-29:242\" dir=\"auto\">The way I see fixing this is either to create a new parameter like <code>omniauth_auto_link_ldap_user</code> that uses the email address instead to link the accounts or extend omni-auth SAML to take in a DN to set <code>extern_uid</code> to the same thing as LDAP.</p>&#x000A;<p data-sourcepos=\"31:1-31:55\" dir=\"auto\">Here is an example user record in the identities table.</p>&#x000A;<pre class=\"code highlight js-syntax-highlight language-plaintext\" lang=\"plaintext\" v-pre=\"true\"><code><span id=\"LC1\" class=\"line\" lang=\"plaintext\">gitlabhq_production=&gt; select * from identities where user_id = 2;</span>&#x000A;<span id=\"LC2\" class=\"line\" lang=\"plaintext\">-[ RECORD 1 ]-------------------------------------------------------</span>&#x000A;<span id=\"LC3\" class=\"line\" lang=\"plaintext\">id         | 5</span>&#x000A;<span id=\"LC4\" class=\"line\" lang=\"plaintext\">extern_uid | CN=LastName\\, FirstName,OU=State,OU=Users,DC=domain,DC=com</span>&#x000A;<span id=\"LC5\" class=\"line\" lang=\"plaintext\">provider   | ldapmain</span>&#x000A;<span id=\"LC6\" class=\"line\" lang=\"plaintext\">user_id    | 2</span>&#x000A;<span id=\"LC7\" class=\"line\" lang=\"plaintext\">created_at |</span>&#x000A;<span id=\"LC8\" class=\"line\" lang=\"plaintext\">updated_at |</span>&#x000A;<span id=\"LC9\" class=\"line\" lang=\"plaintext\">-[ RECORD 2 ]-------------------------------------------------------</span>&#x000A;<span id=\"LC10\" class=\"line\" lang=\"plaintext\">id         | 40</span>&#x000A;<span id=\"LC11\" class=\"line\" lang=\"plaintext\">extern_uid | flastname@domain.com</span>&#x000A;<span id=\"LC12\" class=\"line\" lang=\"plaintext\">provider   | saml</span>&#x000A;<span id=\"LC13\" class=\"line\" lang=\"plaintext\">user_id    | 2</span>&#x000A;<span id=\"LC14\" class=\"line\" lang=\"plaintext\">created_at | 2015-08-03 23:26:37.243648</span>&#x000A;<span id=\"LC15\" class=\"line\" lang=\"plaintext\">updated_at | 2015-08-03 23:26:37.243648</span></code></pre>&#x000A;<p data-sourcepos=\"50:1-50:288\" dir=\"auto\">This way you can clearly see that the <code>extern_uid</code> fields are not equal thus normally gitlab wouldn't set the <code>user_id</code> to be the same. The reason why this record is correctly set, is because I manually attached the saml login to the ldap-created account using the connect to saml button.</p>&#x000A;<p data-sourcepos=\"52:1-52:82\" dir=\"auto\">Please let me know if there is more information I can give to help fix this issue.</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2020-09-02T16:17:51Z",
            "createdAt": "2015-08-04T17:58:46Z",
            "dueDate": null,
            "upvotes": 2,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/209018",
              "avatarUrl": "https://secure.gravatar.com/avatar/c7eff69f671347cc8cfe69c66db1bf95?s=80&d=identicon",
              "username": "agitelzon"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13887",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Deploy Key Notifications Via System Hooks",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:155\" dir=\"auto\">Presently I don't see system hook events raised when a new deploy key is created or destroyed but I do see messages for keys created and removed for users.</p>&#x000A;<p data-sourcepos=\"3:1-3:149\" dir=\"auto\">This is something I will try and implement myself but wanted to add something to track it and see if there are any concerns / issues with doing this.</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2019-11-06T04:22:08Z",
            "createdAt": "2015-08-05T16:53:48Z",
            "dueDate": null,
            "upvotes": 0,
            "downvotes": 0,
            "weight": 6,
            "author": {
              "id": "gid://gitlab/User/136706",
              "avatarUrl": "/uploads/-/system/user/avatar/136706/X_LaU71p.jpeg",
              "username": "stevenorman"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13929",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "When searching in issues and merge requests include the comments",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:47\" dir=\"auto\">Reported by Sven Strickroth of TortoiseGit fame</p>&#x000A;<p data-sourcepos=\"3:1-3:88\" dir=\"auto\">Right now only the title and description is searched, we should search the comments too.</p>&#x000A;<p data-sourcepos=\"5:1-7:108\" dir=\"auto\">For example:&#x000A;When I search for 'git flow' <a href=\"https://gitlab.com/tortoisegit/tortoisegit/issues?utf8=%E2%9C%93&amp;issue_search=git+flow&amp;state=all&amp;scope=all&amp;assignee_id=&amp;author_id=&amp;milestone_id=&amp;label_id=\">https://gitlab.com/tortoisegit/tortoisegit/issues?utf8=%E2%9C%93&amp;issue_search=git+flow&amp;state=all&amp;scope=all&amp;assignee_id=&amp;author_id=&amp;milestone_id=&amp;label_id=</a>&#x000A;I expect to find issue <a href=\"https://gitlab.com/tortoisegit/tortoisegit/issues/992\" data-original=\"https://gitlab.com/tortoisegit/tortoisegit/issues/992\" data-link=\"false\" data-link-reference=\"true\" data-project=\"384627\" data-issue=\"394436\" data-reference-type=\"issue\" data-container=\"body\" data-placement=\"top\" title=\"Add gitflow support\" class=\"gfm gfm-issue has-tooltip\">tortoisegit/tortoisegit#992</a> with 'git flow' in the results</p>&#x000A;<p data-sourcepos=\"9:1-9:97\" dir=\"auto\">Maybe we can use part of <a href=\"http://blog.lostpropertyhq.com/postgres-full-text-search-is-good-enough/\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">http://blog.lostpropertyhq.com/postgres-full-text-search-is-good-enough/</a></p>&#x000A;<p data-sourcepos=\"11:1-11:33\" dir=\"auto\">What do you think <a href=\"/dzaporozhets\" data-user=\"444\" data-reference-type=\"user\" data-container=\"body\" data-placement=\"top\" class=\"gfm gfm-project_member js-user-link\" title=\"Dmitriy Zaporozhets (DZ)\">@dzaporozhets</a> ?</p>&#x000A;<p data-sourcepos=\"13:1-13:20\" dir=\"auto\">/cc <a href=\"/JobV\" data-user=\"5497\" data-reference-type=\"user\" data-container=\"body\" data-placement=\"top\" class=\"gfm gfm-project_member js-user-link\" title=\"Job van der Voort\">@JobV</a> <a href=\"/rspeicher\" data-user=\"15139\" data-reference-type=\"user\" data-container=\"body\" data-placement=\"top\" class=\"gfm gfm-project_member js-user-link\" title=\"Robert Speicher\">@rspeicher</a></p>",
            "totalTimeSpent": 0,
            "updatedAt": "2021-09-24T12:46:13Z",
            "createdAt": "2015-08-05T21:04:00Z",
            "dueDate": null,
            "upvotes": 1,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/1",
              "avatarUrl": "https://secure.gravatar.com/avatar/78b060780d36f51a6763ac9831a4f022?s=80&d=identicon",
              "username": "sytses"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13891",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Creating Archive Failed: File changed as we read it",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:222\" dir=\"auto\">I'm trying to use a windows file server to store the backups from gitlab. During the 6.* versions this worked fine and I'd get a backup tar. In 7.* the backup folder seems to also be the place where the backup is created.</p>&#x000A;<p data-sourcepos=\"3:1-3:67\" dir=\"auto\">when I run the backup routing, it consistently reports this error:</p>&#x000A;<pre class=\"code highlight js-syntax-highlight language-plaintext\" lang=\"plaintext\" v-pre=\"true\"><code><span id=\"LC1\" class=\"line\" lang=\"plaintext\">Creating backup archive: 1439825449_gitlab_backup.tar ... tar: repositories/firmware: file changed as we read it</span>&#x000A;<span id=\"LC2\" class=\"line\" lang=\"plaintext\">tar: repositories: file changed as we read it</span>&#x000A;<span id=\"LC3\" class=\"line\" lang=\"plaintext\">creating archive 1439825449_gitlab_backup.tar failed</span>&#x000A;<span id=\"LC4\" class=\"line\" lang=\"plaintext\">Backup failed</span></code></pre>&#x000A;<p data-sourcepos=\"12:1-12:101\" dir=\"auto\">Would it be possible to define a directory to make the backups and a separate directory for the tars?</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2020-09-02T16:17:46Z",
            "createdAt": "2015-08-17T15:36:44Z",
            "dueDate": null,
            "upvotes": 1,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/154467",
              "avatarUrl": "https://secure.gravatar.com/avatar/8f1eb245b51b1c7c89b1812a60a23e6a?s=80&d=identicon",
              "username": "MeirionHughes"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13927",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Restore backup of empty group does not create the group directory within repositories on local disk",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:314\" dir=\"auto\">When restoring gitlab from a backup which contains an empty group, the group details and members are correctly created, however it does not create the directory on the server for any repositories to be created. The end result is that an error is displayed to the user saying Fork Error...Failed to fork repository.</p>&#x000A;<p data-sourcepos=\"3:1-3:191\" dir=\"auto\">After creating the directory manually on the server within the repositories directory everything continued to work, but really the directory should have been created from the restored backup.</p>&#x000A;<p data-sourcepos=\"5:1-5:97\" dir=\"auto\">Looking at the code, the backups are generated based on projects only, groups are not considered.</p>&#x000A;<p data-sourcepos=\"7:1-7:24\" dir=\"auto\">GitLab Version: <em>7.13.4</em></p>",
            "totalTimeSpent": 0,
            "updatedAt": "2020-09-02T16:17:45Z",
            "createdAt": "2015-08-21T10:43:26Z",
            "dueDate": null,
            "upvotes": 0,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/224891",
              "avatarUrl": "https://secure.gravatar.com/avatar/f196f89106b94d2681575c9b3b9eefe0?s=80&d=identicon",
              "username": "sd-robbruce"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13893",
            "moved": false
          },
          {
            "blockedByCount": 0,
            "title": "Could not authorize you from LDAP because \"Invalid credentials\"  (again)",
            "descriptionHtml": "<p data-sourcepos=\"1:1-1:41\" dir=\"auto\">Hello all, it's my first post on gitab :)</p>&#x000A;<p data-sourcepos=\"3:1-3:40\" dir=\"auto\">Im working with the followings systems :</p>&#x000A;<ul data-sourcepos=\"4:1-6:0\" dir=\"auto\">&#x000A;<li data-sourcepos=\"4:1-4:53\">Red Hat Enterprise Linux Server release 7.1 (Maipo)</li>&#x000A;<li data-sourcepos=\"5:1-6:0\">GITLAB version : gitlab-ce-7.13.5-ce.0.el7.x86_64.rpm</li>&#x000A;</ul>&#x000A;<p data-sourcepos=\"7:1-7:165\" dir=\"auto\">Ive read the issues gitlab-ce#655 gitlab-ce#615 gitlab-ce#639 bit it did not yield anything... First of all, i don't understand why Ive got several Gitlab files in :</p>&#x000A;<ul data-sourcepos=\"8:1-11:0\" dir=\"auto\">&#x000A;<li data-sourcepos=\"8:1-8:48\">/etc/gitlab (in gitlab.rb =&gt; all is commented)</li>&#x000A;<li data-sourcepos=\"9:1-9:47\">/opt/gitlab &lt;= it'a normal place for gitlab ?</li>&#x000A;<li data-sourcepos=\"10:1-11:0\">/var/opt/gitlab</li>&#x000A;</ul>&#x000A;<p data-sourcepos=\"12:1-12:192\" dir=\"auto\">Which config file I have to use ? <code>/etc/gitlab/gitlab.rb</code> or <code>/var/opt/gitlab/gitlab-rails/etc/gitlab.yml</code> ?? I suppose the second one but seeing that Ive got some problems, Ive some doubts :)</p>&#x000A;<p data-sourcepos=\"14:1-14:233\" dir=\"auto\">Secondly, I think I have well-set <code>/var/opt/gitlab/gitlab-rails/etc/gitlab.yml</code> but from the docs, if I do some modifications, I should do a <strong>gitlab-ctl reconfigure</strong> ?? but if I do that, my <strong>gitlab.yml</strong> is reseted, is it normal ?</p>&#x000A;<p data-sourcepos=\"16:1-17:65\" dir=\"auto\">And thridly, Ive got this message when I attempt to connect to the AD server from my Gitlab :&#x000A;<code>Could not authorize you from LDAP because \"Invalid credentials\"</code></p>&#x000A;<p data-sourcepos=\"19:1-19:108\" dir=\"auto\">If I do a <code>gitlab-rake gitlab:ldap:check RAILS_ENV=production</code>, it yields lot of resultat (till 100 I think)</p>&#x000A;<pre class=\"code highlight js-syntax-highlight language-shell\" lang=\"shell\" v-pre=\"true\"><code><span id=\"LC1\" class=\"line\" lang=\"shell\">Checking LDAP ...</span>&#x000A;<span id=\"LC2\" class=\"line\" lang=\"shell\"></span>&#x000A;<span id=\"LC3\" class=\"line\" lang=\"shell\">LDAP <span class=\"nb\">users </span>with access to your GitLab server <span class=\"o\">(</span>only showing the first 100 results<span class=\"o\">)</span></span>&#x000A;<span id=\"LC4\" class=\"line\" lang=\"shell\">Server: ldapmain</span>&#x000A;<span id=\"LC5\" class=\"line\" lang=\"shell\">\tDN: <span class=\"nv\">CN</span><span class=\"o\">=</span>artifactory,OU<span class=\"o\">=</span>Comptes de service,OU<span class=\"o\">=</span>Administration,DC<span class=\"o\">=</span>&lt;my_company_name&gt;,DC<span class=\"o\">=</span>fr\t sAMAccountName: artifactory</span>&#x000A;<span id=\"LC6\" class=\"line\" lang=\"shell\"><span class=\"o\">[</span> lines ... <span class=\"o\">]</span></span>&#x000A;<span id=\"LC7\" class=\"line\" lang=\"shell\">DN: <span class=\"nv\">CN</span><span class=\"o\">=</span>svc_exchange,OU<span class=\"o\">=</span>Comptes de service,OU<span class=\"o\">=</span>Administration,DC<span class=\"o\">=</span>&lt;my_company_name&gt;,DC<span class=\"o\">=</span>fr\t sAMAccountName: svc_exchange</span>&#x000A;<span id=\"LC8\" class=\"line\" lang=\"shell\"></span>&#x000A;<span id=\"LC9\" class=\"line\" lang=\"shell\">Checking LDAP ... Finished</span></code></pre>&#x000A;<p data-sourcepos=\"31:1-31:77\" dir=\"auto\">So far, everything is \"\"ok\"\" but if I do a \"gitlab-ctl tail\" it yields that :</p>&#x000A;<pre class=\"code highlight js-syntax-highlight language-shell\" lang=\"shell\" v-pre=\"true\"><code><span id=\"LC1\" class=\"line\" lang=\"shell\">I, <span class=\"o\">[</span>2015-08-25T12:55:36.726054 <span class=\"c\">#27391]  INFO -- omniauth: (ldapmain) Callback phase initiated.</span></span>&#x000A;<span id=\"LC2\" class=\"line\" lang=\"shell\">E, <span class=\"o\">[</span>2015-08-25T12:55:36.727195 <span class=\"c\">#27391] ERROR -- omniauth: (ldapmain) Authentication failure! missing_credentials encountered.</span></span></code></pre>&#x000A;<p data-sourcepos=\"36:1-36:17\" dir=\"auto\">Idem from the UI.</p>&#x000A;<p data-sourcepos=\"38:1-38:258\" dir=\"auto\">Otherwise, may I bring a credential from the AD server ? The AD admin has given to me a .pdx file (I asked him) and Ive extracted from there the private key that Ive passed into <code>/opt/gitlab/embedded/ssl/certs/&lt;servername&gt;.pem</code> but it yields me a new error :</p>&#x000A;<pre class=\"code highlight js-syntax-highlight language-shell\" lang=\"shell\" v-pre=\"true\"><code><span id=\"LC1\" class=\"line\" lang=\"shell\">I, <span class=\"o\">[</span>2015-08-25T13:47:35.965352 <span class=\"c\">#28600]  INFO -- omniauth: (ldapmain) Callback phase initiated.</span></span>&#x000A;<span id=\"LC2\" class=\"line\" lang=\"shell\">E, <span class=\"o\">[</span>2015-08-25T13:47:35.971833 <span class=\"c\">#28600] ERROR -- omniauth: (ldapmain) Authentication failure! ldap_error: Errno::ECONNRESET, Connection reset by peer @ io_fillbuf - fd:17</span></span></code></pre>&#x000A;<p data-sourcepos=\"43:1-43:67\" dir=\"auto\">Could you guide me on the right way please :D I begin to dispair ;(</p>",
            "totalTimeSpent": 0,
            "updatedAt": "2020-09-02T16:17:42Z",
            "createdAt": "2015-08-25T11:59:00Z",
            "dueDate": null,
            "upvotes": 0,
            "downvotes": 0,
            "weight": null,
            "author": {
              "id": "gid://gitlab/User/228550",
              "avatarUrl": "https://secure.gravatar.com/avatar/dccc05e2a2e3ac0a8c5d543c8c20e643?s=80&d=identicon",
              "username": "Skandar"
            },
            "epic": null,
            "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/issues/13896",
            "moved": false
          }
        ]
      }
    }
  }
}
```
